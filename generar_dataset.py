import numpy as np 
import cv2
from imutils import paths
import os 


# FIJAMOS EL NUEVO ALTO AL QUE QUEREMOS LAS IMAGENES

nuevo_alto = 9


def escala(im, max_alt):
	altura, anchura = im.shape[:2]
	fs = max_alt / float(altura)
	im = cv2.resize(im, None, fx = fs, fy = fs, interpolation = cv2.INTER_AREA)
	return im



def generar_dataset(rutas_imagenes, nuevo_alto):
    for (i, ruta_imagen) in enumerate(rutas_imagenes):
        print("[INFO] procesando imagen {}/{}".format(i + 1,len(rutas_imagenes)))
        clase = ruta_imagen.split(os.path.sep)[-2]
        nombre = ruta_imagen.split(os.path.sep)[-1]			
        img = cv2.imread(ruta_imagen)								
		 

        # hacemos el downgrade

        img_down = escala(img, nuevo_alto)

        # Guardamos la imagen downgradeada

        ruta = "datasets/CAMARA_3/distancia-14m/test/" + clase + "/" + nombre

        cv2.imwrite(ruta, img_down)

rutas_imagenes = list(paths.list_images("datasets/original/test"))
generar_dataset(rutas_imagenes, nuevo_alto)