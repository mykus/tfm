# Script que evalua el sensor del cual se le pase un dataset concreto
import pathlib
from imutils import paths
from librerias.R_Facial import procesar_dataset_SR, procesar_dataset, modelo_vgg_face, test_dist_coseno, cropeador
from librerias.Super_Resolucion import Generador
import cv2

# Fijamos el PATH en el que se encuentra el proyecto
PATH = str(pathlib.Path(__file__).parent.absolute())

# [1] - FASE DE PREPROCESAMIENTO: (Deteccion facial, Recote facial)

# Cargamos el modelo Caffe de deteccion de rostros
detector = cv2.dnn.readNetFromCaffe('recursos/res10/deploy.prototxt','recursos/res10/res10_300x300_ssd_iter_140000.caffemodel')

# Indicamos la ruta de las imagenes en las que se quiere detectar los rostros.
# Conviene cambiar en la ruta la carpeta "test" y "train" para modificar ambos dataset y extraer los rostros de los dos.
rutas_imagenes = list(paths.list_images("datasets/original/test"))

# Cropeamos los rostros detectados en cada imagen.
cropeador(rutas_imagenes, detector)


# [2] - FASE DE CARGA DE DATOS: (Lectura de dataset, Procesado de dataset)

# Generamos el dataset de referencia.
train_imagenes = list(paths.list_images("datasets/original_cropeadas/train")) # Ruta de imagenes a procesar
train_rasgos = "recursos/data/train.pickle" # Ruta donde guardamos el fichero con los datos procesados de los rasgos
train_etiquetas = "recursos/data/train_labels.pickle" # Ruta donde guadamo el fichero con las etiquetas correspondientes a cada rasgo

# Procesamos dataset de referencia.
# Cargamos el modelo de VGG_FACE, si tenemos los pesos entrenados almacenados se lo indicamos por parametro junto a la ruta de los mismos
vgg_face_descriptor = modelo_vgg_face(pesos=True, pesosPath=PATH + '\\recursos\\vgg_face\\vgg_face_weights.h5')

# Procesamos los datos faciales de cada rostro de referencia
procesar_dataset(train_imagenes, train_rasgos, train_etiquetas, vgg_face_descriptor)

# Generamos el dataset de test
# Hay que variar las rutas si se evalua mas de un sensor en el estudio en funcion de las carpetas donde se almacene cada dataset de cada sensor
test_imagenes = list(paths.list_images("datasets/CAMARA_1"))
test_rasgos = list(paths.list_files("datos_procesados/CAMARA_1/rasgos")) # Ruta donde se guardan los archivos binarios de los datos procesados
test_etiquetas = list(paths.list_files("datos_procesados/CAMARA_1/etiquetas")) # Ruta donde se guardan las etiquetas que hacen referencia a los archivos binarios

# Procesamos dataset de test

# Cargamos el generador de rostros por super-resolución
generador = Generador()

# Cargamos los pesos del generado entrenado previamente
generador.load_weights(PATH + "\\recursos\\pesos\\generador.h5")

# Bucle que recorre todas las distancias del dataset y las procesa para extraer los datos faciales a cada distancia
i = 1
for rasgos, etiquetas in zip(test_rasgos, test_etiquetas):
    imagenes = list(paths.list_images("datasets/CAMARA_1/distancia-" + str(i) + "m/test"))
    procesar_dataset_SR(imagenes, rasgos, etiquetas, vgg_face_descriptor, i, generador)
    i += 1


# [3] - FASE DE EVALUACIÓN: (Evaluación del sensor segun su dataset)
print("[INFO] evaluando sistema de identificación...")

# Bucle que calcula la probabilidad de acierto en funcion a la distancia del coseno para todos los datos procesados.
iter = 1
for test, test_lb in zip(test_rasgos, test_etiquetas):
    p_acierto = test_dist_coseno(test, test_lb, train_rasgos, train_etiquetas)
    print("[INFO] la probabilidad de acierto a {} metros es del {:.2f} %".format(iter, p_acierto))
    iter += 1
