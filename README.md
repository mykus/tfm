# Caracterización de sistemas ópticos para vigilancia mediante visión por computador y deep learning

En este repositorio se encuentra el código de mi TFM, el cual consiste en un software que evalua la capacidad de diferentes sistemas ópticos de reconocer mediante inteligencia artificial los rostros de las personas.

Tiene una amplia aplicabilidad en sistemas de calificacion de camaras de vigilancia, ya que los datos que se captan con estas son esenciales en el caso de tener que identificar a ciertos individuos.

El sistema tambien incluye un post-procesado de imagen que aplica Super-Resolución a los rostros para asi poder mejorar la calidad de imagen que se obtiene en cada situacion.

El código principal se lanzaria mediante el fichero "Evaluar_Sensores.py"
