# Libreria para los modelos y herramientas de reconocimiento facial
import os
from keras.models import Model, Sequential
from keras.layers import Convolution2D, ZeroPadding2D, MaxPooling2D, Flatten, Dense, Dropout, Activation
from keras.preprocessing.image import img_to_array
from keras.applications.imagenet_utils import preprocess_input
import numpy as np
import pickle
from librerias.Super_Resolucion import mejorar, reescala
import cv2

# Función que detecta los rostros en una imagen dada.
def detectar(imagen, detector):
    (h, w) = imagen.shape[:2]
    # Detectamos la cara en la imagen cargada
    # Para ello, construimos un blob con las caracteristicas del detector de rostros
    # de la imagen cargada

    blob_img = cv2.dnn.blobFromImage(cv2.resize(imagen, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0),
                                     swapRB=False, crop=False)

    # Ahora le pasamos el blob al detector para localizar las coordenadas donde se encuentra la cara

    detector.setInput(blob_img)
    detectado = detector.forward()

    # Con los datos obtenidos revisamos que sean rostros validos para asi
    # poder trabajar con ellos

    if len(detectado) > 0:

        # Extremos la posicion con las coordenadas del sector de la foto en la que se encuentra la maxima probabilidad de tener un rostro en su
        # interior
        max_prob = np.argmax(detectado[0, 0, :, 2])
        probabilidad = detectado[0, 0, max_prob, 2]

        # Si cumple una probabilidad mínima de que sea un rostro se procede a su procesamiento
        if probabilidad > 0.5:

            # Extraemos las coordenadas de la zona en la que se encuentra el rostro, para ello multiplicamos lo que nos devuelve la red con un vector de ceros
            # de tipo entero para convertirlo a numeros enteros ya que la red nos lo devuelve en tipo char.
            (X, Y, W, H) = (detectado[0, 0, max_prob, 3:7] * np.array([w, h, w, h])).astype('int')

        else:
            # En el caso de no detectar nada ponemos un recuadro estandar para que no se produzcan errores
            (X, Y, W, H) = (0, 0, w, h)

    return X, Y, W, H

# Funcion que recorta las caras de una serie de imagenes en una ruta indicada
def cropeador(rutas_imagenes, detector):
    # Bucle que recorre imagen a imagen buscando rostros en ella y recortando los que detecta
    for (i, ruta_imagen) in enumerate(rutas_imagenes):
        print("[INFO] procesando imagen {}/{}".format(i + 1, len(rutas_imagenes)))
        clase = ruta_imagen.split(os.path.sep)[-2]
        nombre = ruta_imagen.split(os.path.sep)[-1]
        img = cv2.imread(ruta_imagen)   # Cargamos la imagen de la ruta
        (X, Y, W, H) = detectar(img, detector)  # Llamamos a la fucnion que detecta las coordenadas del rostro

        if(X != 0 and Y != 0):  # Comprobamos que las coordenadas sean validas
            cara = img[Y:H, X:W]    # Recortamos la region del rostro
            ruta = "datasets/original_cropeadas/test/" + clase + "/" + nombre   # Creamos la nueva ruta para las imagenes extraidas
            cv2.imwrite(ruta, cara) # Guardamos la nueva imagen


# Función que monta la arquitectura de la red
def modelo_vgg_face(pesos = False, pesosPath = ''):
    # Describimos la arquitectura de la red VGG_face
    vgg_face = Sequential()
    vgg_face.add(ZeroPadding2D((1,1),input_shape=(224,224, 3)))
    vgg_face.add(Convolution2D(64, (3, 3), activation='relu'))
    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(64, (3, 3), activation='relu'))
    vgg_face.add(MaxPooling2D((2,2), strides=(2,2)))

    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(128, (3, 3), activation='relu'))
    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(128, (3, 3), activation='relu'))
    vgg_face.add(MaxPooling2D((2,2), strides=(2,2)))

    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(256, (3, 3), activation='relu'))
    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(256, (3, 3), activation='relu'))
    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(256, (3, 3), activation='relu'))
    vgg_face.add(MaxPooling2D((2,2), strides=(2,2)))

    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(512, (3, 3), activation='relu'))
    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(512, (3, 3), activation='relu'))
    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(512, (3, 3), activation='relu'))
    vgg_face.add(MaxPooling2D((2,2), strides=(2,2)))

    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(512, (3, 3), activation='relu'))
    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(512, (3, 3), activation='relu'))
    vgg_face.add(ZeroPadding2D((1,1)))
    vgg_face.add(Convolution2D(512, (3, 3), activation='relu'))
    vgg_face.add(MaxPooling2D((2,2), strides=(2,2)))

    vgg_face.add(Convolution2D(4096, (7, 7), activation='relu'))
    vgg_face.add(Dropout(0.5))
    vgg_face.add(Convolution2D(4096, (1, 1), activation='relu'))
    vgg_face.add(Dropout(0.5))
    vgg_face.add(Convolution2D(2622, (1, 1)))
    vgg_face.add(Flatten())
    vgg_face.add(Activation('softmax'))

    if pesos:
        # Cargamos los pesos entrenados de la red
        vgg_face.load_weights(pesosPath)

    # Creamos el modelo
    vgg_face_descriptor = Model(inputs=vgg_face.layers[0].input, outputs=vgg_face.layers[-2].output)

    return vgg_face_descriptor

#Función que prepara a las imagenes para entrar en la red
def preprocesar_vgg_face(imagen):
    img = cv2.resize(imagen, (224, 224))    # Le damos el tamaño necesario que pide la red
    img = img_to_array(img)                 # Converitmos los datos de la imagen a un array
    img = np.expand_dims(img, axis=0)       # Añadimos una columna vacia para darle el tamaño que pide la red
    img = preprocess_input(img)             # Añadimos un preprocesado de Keras necesario para la arquitectura de la red
    return img

def preprocesar_vgg_face_SR(imagen):
    img = reescala(imagen, 224, 224)
    img = img_to_array(img)
    img = np.expand_dims(img, axis=0)
    return img

# Función que lee el dataset que se quiere procesar para evaluar y aplica Super-Resolución (se usa para el dataset de test)
def procesar_dataset_SR(rutas_imagenes, lib_rasgos, lib_labels, vgg_face_descriptor, iteracion, generador):
    print('[INFO] leyendo dataset...')

    # Inicializamos las listas de rasgos y nombres de cada clase, se usa cada vez que añadimo un nuevo usuario al sistema de verificación
    rasgos_detectados = []
    id_actual = 0
    labels = {}
    for (i, ruta_imagen) in enumerate(rutas_imagenes):
        print("[INFO] Distancia {} metros, procesando imagen {}/{}".format(iteracion, i + 1, len(rutas_imagenes)))
        nombre = ruta_imagen.split(os.path.sep)[-2]  # Extraemos el nombre de la clase que esta contenido en la carpeta donde se almacena la foto de la clase

        # Aplicamos super-resolucion
        img_sr = mejorar(generador, ruta_imagen)

        # Sacamos el vector de caracteristicas
        vector_rasgos = vgg_face_descriptor.predict(preprocesar_vgg_face_SR(img_sr))
        labels[id_actual] = nombre
        id_actual += 1
        rasgos_detectados.append(vector_rasgos.flatten())

    # Creamos un diccionario con cada nombre y su rasgo conocido
    base_datos = rasgos_detectados
    # Guardando base de datos
    with open(lib_labels, 'wb') as f:
        pickle.dump(labels, f)

    with open(lib_rasgos, 'wb') as f:
        pickle.dump(base_datos, f)

    print('[INFO] dataset guardado...')

# Función que lee el dataset que se quiere procesar para evaluar pero NO aplica Super-Resolución (se usa para el dataset de referencia)
def procesar_dataset(rutas_imagenes, lib_rasgos, lib_labels, vgg_face_descriptor):
    print('[INFO] leyendo dataset...')

    # Inicializamos las listas de rasgos y nombres de cada clase, se usa cada vez que añadimo un nuevo usuario al sistema de verificación
    rasgos_detectados = []
    id_actual = 0
    labels = {}
    for (i, ruta_imagen) in enumerate(rutas_imagenes):
        print("[INFO] procesando imagen {}/{}".format(i + 1, len(rutas_imagenes)))
        nombre = ruta_imagen.split(os.path.sep)[-2]  # Extraemos el nombre de la clase que esta contenido en la carpeta donde se almacena la foto de la clase
        img = cv2.imread(ruta_imagen)  # Cargamos la imagen que vamos a analizar

        # Sacamos el vector de caracteristicas
        vector_rasgos = vgg_face_descriptor.predict(preprocesar_vgg_face(img))
        labels[id_actual] = nombre
        id_actual += 1
        rasgos_detectados.append(vector_rasgos.flatten())

    # Creamos un diccionario con cada nombre y su rasgo conocido
    base_datos = rasgos_detectados
    # Guardando base de datos
    with open(lib_labels, 'wb') as f:
        pickle.dump(labels, f)

    with open(lib_rasgos, 'wb') as f:
        pickle.dump(base_datos, f)

    print('[INFO] dataset guardado...')

# Función para calcular la distancia del coseno
def dist_coseno(vector_ref, vector_input):
    a = np.matmul(np.transpose(vector_ref), vector_input)
    b = np.sum(np.multiply(vector_ref, vector_ref))
    c = np.sum(np.multiply(vector_input, vector_input))
    distancia = 1 - (a / (np.sqrt(b) * np.sqrt(c)))
    return distancia

# Función que testea los aciertos de un modelo según la distancia del coseno
def test_dist_coseno(vectores_test, etiquetas_test, vectores_ref, etiquetas_ref):
    # Cargamos los valores de referencia
    with open(vectores_ref, 'rb') as f:
        caras_conocidas = pickle.load(f)
    with open(etiquetas_ref, 'rb') as f:
        nom_ref = pickle.load(f)
    # Cargamos los valores de test
    with open(vectores_test, 'rb') as f:
        caras_test = pickle.load(f)
    with open(etiquetas_test, 'rb') as f:
        nom_test = pickle.load(f)

    aciertos = 0
    for i in range(len(caras_test)):
        vector_test = caras_test[i]
        nombre_test = nom_test[i]
        dist_min = 10       # Fijamos una distancia minima alta para analizar el dataset entero y quedarnos con la mas baja.

        # Calculamos la distancia minima con los de referencia para ver que nombre le asigna
        for j in range(len(caras_conocidas)):
            cara = caras_conocidas[j]
            distancia = dist_coseno(cara, vector_test)
            if distancia < dist_min:
                dist_min = distancia
                id_reconocido = j
        if (nombre_test == nom_ref[id_reconocido]):
            aciertos += 1

    p_aciertos = (aciertos / (i + 1)) * 100

    return p_aciertos
