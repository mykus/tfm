# Script que contiene las funciones basicas para el modelo de super-resolución
import tensorflow as tf
import pathlib
from tensorflow.keras import *
from tensorflow.keras.layers import *


# Añadimos las rutas de entrada de las diferentes partes del dataset
PATH = str(pathlib.Path(__file__).parent.absolute())
PESOS_PATH = PATH + "\\pesos"

# Función que reescala las imagenes al tamaño indicado
def reescala(in_img, width, height):
    in_img = tf.image.resize(in_img, [height, width])
    return in_img

# Función que normaliza las imagenes entre -1 y 1.
def normaliza(in_image):
    in_image = (in_image / 127.5) - 1
    return in_image

# Funcion para reescalar las imagenes

def reescala_SR(in_img, out_img, width, height):
    in_img = tf.image.resize(in_img, [height, width])
    out_img = tf.image.resize(out_img, [height, width])
    return in_img, out_img

# Funcion que normaliza las imagenes para hacer la computacion mas estable

def normaliza_SR(in_image, out_image):
    in_image = (in_image / 127.5) - 1
    out_image = (out_image / 127.5) - 1
    return in_image, out_image


# Funcion recomendada en el paper Pix2Pix para hacer aumentacion de datos
# Es decir, generamos mas datos de nuestro dataset para asi mejorar el entrenamiento

def random_jitter(in_image, out_image, img_width, img_height):
    # Hacemos las imagenes un poco mas grandes de lo que eran para poder recortar una parte con el tamaño deseado para la red
    in_image, out_image = reescala_SR(in_image, out_image, 286, 286)

    # Stackeamos las imagenes tanto de input como de output en un solo bloque para poder recortarlas todas juntas
    stacked_image = tf.stack([in_image, out_image], axis=0)
    # Recortamos las imagenes aumentadas en parches aleatorios del tamaño definido para la red
    cropped_image = tf.image.random_crop(stacked_image, size=[2, img_height, img_width, 3])

    # Deshacemos el stack anterior para recuperar las de input y las de output
    in_image, out_image = cropped_image[0], cropped_image[1]

    # De forma aleatoria y con un 50 % de probabilidad flipeamos las imagenes recortadas
    if tf.random.uniform(()) > 0.5:
        in_image = tf.image.flip_left_right(in_image)
        out_image = tf.image.flip_left_right(out_image)

    return in_image, out_image


# Para agilizar el codigo, observamos el paper y vemos que en la estructura de la red esta repide
# una serie de bloques de capas, es por eso que ahora vamos a definir esos bloques para poder reutilizaros
# a lo largo del diseño de la red.

# Función que crea un bloque de la rama de downsampling de la U-net.
def downsample(filtros, aplicar_batch=True):
    # Iniciamos la red
    net = Sequential()

    # Creamos un iniciador de los pesos de la red con un ruido aleatorio gaussiano
    iniciador = tf.random_normal_initializer(0, 0.02)

    # Capa convolucional
    net.add(Conv2D(filtros,
                   kernel_size=4,  # Definimos un tamaño de kernel de 4 x 4
                   strides=2,  # Definimos el avance del kernel de 2 en 2 pixeles
                   padding="same",
                   # Definimos el padding de forma igual para conservar eltamaño de los mapas de caracteristicas segun avanzan por las capas
                   kernel_initializer=iniciador,  # Iniciamos los pesos con el inicializador
                   use_bias=not aplicar_batch
                   # Aplicamos el bias cuando no usamos el batch para que el batch introduzca una serie de hiperparametros refernetes a la capa anterior la cual no tendria batch, es por eso que le añadimos el bias
                   ))

    if aplicar_batch:
        # Capa BatchNorm
        net.add(BatchNormalization())

    # Capa de activacion
    net.add(LeakyReLU())

    return net

# Función que crea un bloque de la rama de upsampling de la U-net.
def upsample(filtros, aplicar_Dropout=False):
    # Iniciamos la red
    net = Sequential()

    # Creamos un iniciador de los pesos de la red con un ruido aleatorio gaussiano
    iniciador = tf.random_normal_initializer(0, 0.02)

    # Capa convolucional
    net.add(Conv2DTranspose(filtros,
                            kernel_size=4,
                            strides=2,
                            padding="same",
                            kernel_initializer=iniciador,
                            use_bias=False
                            ))

    # Capa BatchNorm
    net.add(BatchNormalization())

    if aplicar_Dropout:
        # Capa de Dropout
        net.add(Dropout(0.5))

    # Capa de activacion
    net.add(ReLU())

    return net


# Vamos a definir la funcion que crea la parte llamada GENERADOR en esta arquitectura de red neuronal

def Generador():
    # Creamos la capa de entrada
    entrada = tf.keras.layers.Input(shape=[None, None, 3])

    # Ahora vamos a crear un vector de capas que conformen el encoder

    encoder = [
        downsample(64, aplicar_batch=False),  # (bs, 128, 128, 64)
        downsample(128),  # (bs, 64,  64,  128)
        downsample(256),  # (bs, 32,  32,  128)
        downsample(512),  # (bs, 16,  16,  512)
        downsample(512),  # (bs, 8,   8,   512)
        downsample(512),  # (bs, 4,   4,   512)
        downsample(512),  # (bs, 2,   2,   512)
        downsample(512)  # (bs, 1,   1,   512)
    ]

    # Ahora vamos a crear el vector del decoder
    decoder = [
        upsample(512, aplicar_Dropout=True),  # (bs, 2,   2,   1024)
        upsample(512, aplicar_Dropout=True),  # (bs, 4,   4,   1024)
        upsample(512, aplicar_Dropout=True),  # (bs, 8,   8,   1024)
        upsample(512),  # (bs, 16,  16,  1024)
        upsample(256),  # (bs, 32,  32,  512)
        upsample(128),  # (bs, 64,  64,  256)
        upsample(64),  # (bs, 128, 128, 128)
    ]

    # Volvemos a crear un iniciador para los pesos de las capas finales
    iniciador = tf.random_normal_initializer(0, 0.02)

    salida = Conv2DTranspose(filters=3,  # Los tres canales de color de la imagen generada
                             kernel_size=4,
                             strides=2,
                             padding="same",
                             kernel_initializer=iniciador,
                             activation="tanh"
                             # Usamos la tangente hiperbólica ya que es una funcion que trabaja en el domino entre -1 y 1 como nuestras imagenes
                             )

    # Iniciamos el montaje de la red
    cGAN = entrada

    # Iniciamos una lista para concatenar cada capa del encoder con su consecuente en el decoder
    # de esta forma creamos la topologia en U.
    # Formamos las Skip-Connections
    skip = []
    concat = Concatenate()

    # Con este bucle conseguimos concatenar todas las capas del encoder
    for enc in encoder:
        cGAN = enc(cGAN)
        skip.append(cGAN)

    # Invertimos la lista de skips para añadirla al decoder, tambien eliminamos el ultimo elemento ya que corresponde con el cuello de botella
    skip = reversed(skip[:-1])

    # Con este bucle conseguimos concatenar todas las capas del decoder
    for dec, sk in zip(decoder, skip):
        cGAN = dec(cGAN)
        cGAN = concat([cGAN, sk])

    # Finalmente concatenamos la ultima capa, la de salida
    salida = salida(cGAN)

    return tf.keras.Model(inputs=entrada, outputs=salida)


# A continuacion, vamos a crear el discriminador para verificar si las imagenes generadas son reales o no

def Discriminador():
    entrada_org = Input(shape=[None, None, 3], name="entrada_org")
    im_generada = Input(shape=[None, None, 3], name="im_generada")

    # Al tener dos inputs tenemos que apilarlos en un stack para que la red funcione

    entrada = concatenate([entrada_org, im_generada])

    # Creamos el iniciador de los valores de los pesos

    iniciador = tf.random_normal_initializer(0, 0.02)

    # Creamos el vector de capas del discriminador

    D = [
        downsample(64, aplicar_batch=False),  # (bs, 128, 128, 64)
        downsample(128),  # (bs, 64,  64,  128)
        downsample(256),  # (bs, 32,  32,  256)
        downsample(512)  # (bs, 16,  16,  512)
    ]

    # Definimos la capa de salida

    salida = tf.keras.layers.Conv2D(filters=1,
                                    kernel_size=4,
                                    strides=1,
                                    kernel_initializer=iniciador,
                                    padding="same"
                                    )

    # Iniciamos el montaje de la red
    PatchGAN = entrada

    # Hacemos un bucle para concatenar las capas

    for P in D:
        PatchGAN = P(PatchGAN)

    # Finalmente añadimos la capa final

    salida = salida(PatchGAN)

    return tf.keras.Model(inputs=[entrada_org, im_generada], outputs=salida)

# Ahora vamos a crear las funciones de perdida tanto para el generador como para el discriminador
# Estas funciones nos serviran a la hora de entrenar para conocer el estado de aprendizaje en el que
# se encuentra nuestra red y ademas para poder enfrentarlas entre ellas.

# Declaramos el objeto de perdida para calcular la entropia cruzada.

perdida = tf.keras.losses.BinaryCrossentropy(from_logits = True)

def perdida_discriminador(disc_real_out, disc_gen_out):
    # Diferencia entre los verdaderos por ser reales y el detectado por el discretizador
    perdida_real = perdida(tf.ones_like(disc_real_out), disc_real_out)

    # Diferencia entre los falsos por ser generados y los detectados por el discretizador
    perdida_gen = perdida(tf.zeros_like(disc_gen_out), disc_gen_out)

    perdida_total_disc = perdida_real + perdida_gen

    return perdida_total_disc

# Vamos a crear la funcion de perdida del generador, la cual va a competir con la del discretizador
# para intentar auentar el valor de error de este, es decir, engañarlo.

LAMBDA = 100 # Hiperparametro definido por el paper para dar mas peso al error L1 que al del generador

def perdida_generador(disc_gen_out, gen_out, objetivo):
    # Aqui es donde conectamos el error del discretizador con el del generador para que compitan, ya que ahora
    # vamos a comparar la salida del discretizador sobre la ultima imagen generada, con el resultado que desamos que
    # tenga, sease que nos dijera que la imagen es real (todo unos)

    perdida_gen_deseada = perdida(tf.ones_like(disc_gen_out), disc_gen_out)

    # Calculamos el error cuadratico medio entre los pixeles de la imagen generada y la imagen real que queriamos conseguir
    perdida_L1 = tf.reduce_mean(tf.abs(objetivo - gen_out))

    perdida_total_gen = perdida_gen_deseada + (LAMBDA * perdida_L1)

    return perdida_total_gen


# Función que aplica la super-resolucion a la imagen de entrada que se le indique
def mejorar(model, path_in_image, save=False):
    in_image = tf.cast(tf.image.decode_jpeg(tf.io.read_file(path_in_image)), tf.float32)[..., :3]
    in_image = in_image[None, ...]
    in_image = reescala(in_image, 256, 256)
    in_image = normaliza(in_image)
    out_image = model(in_image, training=True)

    if save:
        tf.keras.preprocessing.image.save_img("output.jpg", out_image[0, ...])


    return out_image[0, ...]

