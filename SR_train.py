# Script que entrena el modelo de super-resolución.
import tensorflow as tf
import numpy as np
import os
import pathlib
import matplotlib.pyplot as plt
from IPython.display import clear_output
from librerias.Super_Resolucion import reescala_SR, random_jitter, normaliza_SR
from librerias.Super_Resolucion import Generador, Discriminador, perdida_discriminador, perdida_generador

# Añadimos las rutas de entrada de las diferentes partes del dataset
PATH = str(pathlib.Path(__file__).parent.absolute())
INPUT_PATH = PATH + "\\SR_data\\bin\\inputs"
OUTPUT_PATH = PATH + "\\SR_data\\bin\\outputs"
# Guardamos la ruta donde se van a almacenar los avances de nuestra
# red conforme se vaya entrenando
CHECK_PATH = PATH + "\\SR_data\\bin\\checks"

# Cogemos los nombres de los archivos del directorio INPUT_PATH
imgnames = list(os.listdir(INPUT_PATH))

# En linux seria de la siguiente manera
# imgurls = !ls -1 "{INPUT_PATH}"

# Vamos a preparar el dataset

# Definimos el numero de imagenes con el que vamos a trabajar
n = 750 # El dataset entero

# Seleccionamos la parte del dataset que vamos a usar de entrenamiento, en este caso un 80 %
n_train = round(n * 0.80)

# Vamos a barajar los nombres de las imagenes para que estos nos salgan desordenados
rngnames = np.copy(imgnames)
np.random.shuffle(rngnames)

# Hacemos la particion train/test
train_names = rngnames[:n_train]
test_names = rngnames[n_train:n]

# Ploteamos los resultados
#print(len(imgnames), len(train_names), len(test_names))

IMG_WIDTH = 256
IMG_HEIGHT = 256


# Vamos a crear las funciones encargadas de cargar las imagenes usando las funciones que acabamos de crear

def load_images(filename, aumentar=True):
    # Cargamos las imagenes en tensorflow
    in_image = tf.cast(tf.image.decode_jpeg(tf.io.read_file(INPUT_PATH + '/' + filename)), tf.float32)[..., :3]
    out_image = tf.cast(tf.image.decode_jpeg(tf.io.read_file(OUTPUT_PATH + '/' + filename)), tf.float32)[..., :3]

    in_image, out_image = reescala_SR(in_image, out_image, IMG_WIDTH, IMG_HEIGHT)

    if aumentar:
        in_image, out_image = random_jitter(in_image, out_image, IMG_WIDTH, IMG_HEIGHT)

    in_image, out_image = normaliza_SR(in_image, out_image)

    return in_image, out_image


# Especificamos dos funciones para la carga de imagenes de entrenamiento y de test

def load_train_images(filename):
    return load_images(filename, True)


def load_test_images(filename):
    return load_images(filename, False)

# Comenzamos la carga de datos en los tensores de TensorFlow

train_dataset = tf.data.Dataset.from_tensor_slices(train_names)
train_dataset = train_dataset.map(load_train_images, num_parallel_calls = tf.data.experimental.AUTOTUNE)
train_dataset = train_dataset.batch(1)

test_dataset = tf.data.Dataset.from_tensor_slices(test_names)
test_dataset = test_dataset.map(load_test_images, num_parallel_calls = tf.data.experimental.AUTOTUNE)
test_dataset = test_dataset.batch(1)

# Hasta aqui el tratamiento de datos previos al entrenamiento, a partir de aqui vamos a cargar la red


# Iniciamos tanto el GENERADOR como el DISCRIMINADOR
generador = Generador()

# Ploteamos la arquitectura del generador
#tf.keras.utils.plot_model(generador, show_shapes=True, dpi=64)

discriminador = Discriminador()

# Ploteamos la arquitectura del discriminador
#tf.keras.utils.plot_model(discriminador, show_shapes=True, dpi=64)


# Esta parte define los optimizadores a usar para el discriminador y el generador, asi como las forma de
# guardar los checkpoints de la red.

opti_generador = tf.keras.optimizers.Adam(2e-4, beta_1 = 0.5)
opti_discriminador = tf.keras.optimizers.Adam(2e-4, beta_1 = 0.5)

pref_check = os.path.join(CHECK_PATH, "ckpt")

checkpoint = tf.train.Checkpoint(generator_optimizer = opti_generador,
                                 discriminator_optimizer = opti_discriminador,
                                 generator = generador,
                                 discriminator = discriminador
                                 )

# Para recuperar el ultimo chechpoint se usaria esto:

#checkpoint.restore(tf.train.latest_checkpoint(CHECK_PATH)).assert_consumed()

# Funcion para comprobar el funcionamiento del modelo generador durante el entrenamiento

def generar_imagenes(model, input, objetivo, guardar=False, mostrar=True):
    # Hacemos la prediccion del input
    prediccion = model(input, training=True)

    if guardar:
        tf.keras.preprocessing.image.save_img(PATH + '/SR_data/testeos/' + guardar + '.jpg', prediccion[0, ...])

    plt.figure(figsize=(10, 10))

    if mostrar:
        display_list = [input[0], objetivo[0], prediccion[0]]
        titulo = ['Imagen de Entrada', 'Imagen Real', 'Imagen Generada']
        for i in range(3):
            plt.subplot(1, 3, i + 1)
            plt.title(titulo[i])
            plt.imshow(display_list[i] * 0.5 + 0.5)
            plt.axis('off')
        plt.show()


# Creamos el modelo de entrenamiento conectando todos los bloques anteriores
# Le añadimos el decorador @tf.function() paa que tensorflow aplique AutoGrad
# que consiste en una opcion de esta libreria que optimiza el codigo generando una
# grafo computacional haciendo lo mas eficiente posible el computo de la red.
@tf.function()
def train_step(imagen_entrada, objetivo):
    # Iniciamos los objetos que calculan automaticamente los gradientes del paso ejecutado
    # para asi poder aplicar el backpropagation
    with tf.GradientTape() as gen_tape, tf.GradientTape() as discr_tape:
        # Generamos una imagen en funcion a la de entrada
        imagen_gen = generador(imagen_entrada, training = True)
        # Generamos la imagen del discriminador comparando con la generada
        out_gen_discr = discriminador([imagen_gen, imagen_entrada], training = True)
        # Generamos la imagen del discriminador comparando con la ideal
        out_obj_discr = discriminador([objetivo, imagen_entrada], training = True)

        # Calculamos las perdidas de ambos bloques
        perdida_discr = perdida_discriminador(out_obj_discr, out_gen_discr)

        perdida_gen = perdida_generador(out_gen_discr, imagen_gen, objetivo)

        # Calculamos los gradientes que se generan con las salidas de este paso

        gradiente_G = gen_tape.gradient(perdida_gen, generador.trainable_variables)
        gradiente_D = discr_tape.gradient(perdida_discr, discriminador.trainable_variables)

        # Aplicamos los gradientes calculados a los optimizadores definidos anteriormente

        opti_generador.apply_gradients(zip(gradiente_G, generador.trainable_variables))
        opti_discriminador.apply_gradients(zip(gradiente_D, discriminador.trainable_variables))


# Definimos la funcion con la rutina de entrenamiento

def train(dataset, epocas):
    for epoca in range(epocas):
        imgn = 0

        # Recorremos el dataset entrenando el modelo segun el numero de epocas definido
        for imagen_entrada, objetivo in dataset:
            print('epoca: ' + str(epoca) + ' - train: ' + str(imgn + 1) + '/' + str(len(train_names)))
            imgn += 1
            train_step(imagen_entrada, objetivo)
            clear_output(wait = True)

        imgn = 0
        # Vamos probando imagenes del test y guardandolas para observar como va progresando el modelo con imagenes nunca vistas
        for entrada, objt in test_dataset.take(3):
            generar_imagenes(generador, entrada, objt, str(imgn) + '_' + str(epoca), mostrar=False)
            imgn += 1

        # Guardamos un checkpoint cada 10 epocas
        if (epoca + 1) % 10 == 0:
            checkpoint.save(file_prefix = pref_check)

# Finalmente podemos llamar a la funcion train() para comenzar el entrenamento de la red.

#checkpoint.restore(tf.train.latest_checkpoint(CHECK_PATH))

train(train_dataset, 100)
